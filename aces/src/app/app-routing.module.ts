import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { GamesComponent } from './games/games.component';
import { ClubsComponent } from './clubs/clubs.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { DivisionsComponent } from './divisions/divisions.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
 {path: 'home', component: HomeComponent},
 {path: 'games', component: GamesComponent},
 {path: 'clubs', component: ClubsComponent},
 {path: 'statistics', component: StatisticsComponent},
 {path: 'divisions', component: DivisionsComponent},




];

@NgModule({
 imports: [RouterModule.forRoot(routes,)],
 exports: [RouterModule]
})
export class AppRoutingModule {

}
