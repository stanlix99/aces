import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-divisions',
  templateUrl: './divisions.component.html',
  styleUrls: ['./divisions.component.scss']
})
export class DivisionsComponent implements OnInit {

  constructor() { }
  public games: any[] = [{title:"1 DIVISION",image:"https://www.volkgames.com/wp-content/uploads/2020/05/valorant.png"},{title:"2 DIVISION",image:"https://cdn2.unrealengine.com/metaimage1-1920x1080-abb60090deaf.png"},{title:"2 DIVISION",image:"https://theme.zdassets.com/theme_assets/43400/87a1ef48e43b8cf114017e3ad51b801951b20fcf.jpg"}];

  ngOnInit(): void {
  }

}
